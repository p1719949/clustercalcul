Cluster creator est une application créatrice de cluster utilisant un serveur local en express js.

L'application utilise des scripts python pour créer des machines virtuels sur des host(i.e amazon, openstack) et lient les machines ainsi créées en cluster.

Ensuite l'utilisateur peut utiliser les fonctions de gestion pour stopper les machines du cluster.

Il est aussi possible de lancer des applications sur le cluster en donnant le lien du git de l'application dans l'onglet ajouter une application.

Prérequis: ubuntu, python3, git

Fichiers nécéssaires: 
 - credentialsCloud.csv: de la forme nomhebergeur, username, password
 - MyFirstECTwo.pub et MyFirstECTwo.pem: clés ssh utilisés

Mise en place du serveur:

- git clone à completer

- python3 scriptInstallationServeur.py

Lancement du serveur:

- cd siteweb

- node server.js

Accéder à l'interface web à l'adresse localhost:8080






