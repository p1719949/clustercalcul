import os

# Recuperation du nom du repo

fichiersPresents = os.listdir(".")
# print(fichiersPresents)
dossiersPresents = []
for fichier in fichiersPresents:
    if os.path.isdir(fichier):
        dossiersPresents.append(fichier)
# print(dossiersPresents)
nomRepo = dossiersPresents[0] # Le seul dossier existant dans le current directory est le repo clone

# Suppression du deployment
cmdDeleteDeployment = "kubectl delete deployment " + nomRepo
os.system(cmdDeleteDeployment)

# Suppression du dossier du repo
cmdDeleteDossier = "rm -rf " + nomRepo
os.system(cmdDeleteDossier)