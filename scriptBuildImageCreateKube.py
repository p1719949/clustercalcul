import os
import os.path
import re
import time
import sys
import json
import subprocess

# Recuperation du json

print("Args:", sys.argv)

if len(sys.argv) != 2:
    raise RuntimeError("Nombre d'arguments différents de 2")

requestData = json.loads(sys.argv[1])

print("Arg: ", requestData)

# Exemple du json
requestData = {
    "hebergeur": "ec2",
    "nomCluster": "testCluster1",
    "public_ips": [""],
    "private_ips": [""],
    "dockerImageAvailableOnline": 1,
    "gitRepositoryLink": "",
    "kubeYAMLFileLocations": [""],
    "gitUsername": "",
    "gitPassword": ""
}

# Recuperation du git repository avec les fichiers necessaires

# source_dir= "https://forge.univ-lyon1.fr/p1719949/clustercalcul"
dest_dir= "/home/ubuntu/gitRepos"

# Recuperation du nom du repo
nomRepo = requestData["gitRepositoryLink"][0:-4]
nomRepo = nomRepo.split("/")[len(nomRepo.split("/")) - 1]

repoDejaClone = False
fichiersPresents = os.listdir(dest_dir)
dossiersPresents = []
for fichier in fichiersPresents:
    if os.path.isdir(fichier):
        if fichier == nomRepo:
            repoDejaClone = True
        
if requestData["gitUsername"] != "":
    cloneInput = requestData["gitUsername"] + "\n" + requestData["gitPassword"] 
else:
    cloneInput = ""
cloneProcess = subprocess.run(["git", "clone", requestData["gitRepositoryLink"]], input=cloneInput, 
stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf8", timeout=60)
cloneProcess.check_returncode()

# fichiersPresents = os.listdir(dest_dir)
# # print(fichiersPresents)
# dossiersPresents = []
# for fichier in fichiersPresents:
#     if os.path.isdir(fichier):
#         dossiersPresents.append(fichier)
# # print(dossiersPresents)
# nomRepo = dossiersPresents[0] # Le seul dossier existant dans le current directory est le repo clone

# Recherche du Dockerfile dans le repo
if not requestData["dockerImageAvailableOnline"]:
    try: 
        os.path.isfile("/Dockerfile")
    except:
        print("Erreur du script: Dockerfile pas trouvé.")
        cmd_rm_repo="rm -rf " + nomRepo # TODO: check if -f is necessary
        os.system(cmd_rm_repo)
    else:
        print("Dockerfile trouve")

    # Build image à partir du Dockerfile

    # Push du dockerfile dans le registry

# Lancement de kubernetes à partir de l'image 

for kubeYAMLFileLocation1 in requestData["kubeYAMLFileLocations"]:
    kubeYAMLFileLocation2 = kubeYAMLFileLocation1
    # TODO: check if ./ is there
    os.system("python3 ./scriptKubeAPI.py create --fileLocation=" + "./" + kubeYAMLFileLocation2)
# time.sleep(6000)