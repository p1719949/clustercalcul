import paramiko, yaml, sys, json

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.base import NodeAuthSSHKey
from libcloud.compute.deployment import SSHKeyDeployment, FileDeployment, ScriptDeployment, MultiStepDeployment

# Parametres à recuperer depuis le site:

print("Args:", sys.argv)

if len(sys.argv) != 2:
    raise RuntimeError("Nombre d'arguments différents de 2")

clusterConfigData = json.loads(sys.argv[1])

print("Arg: ", clusterConfigData)

# HEBERGEUR = 'ec2'
# ec2, openstack
# Exemple:
# clusterConfigData = {
#     "nom": "testCluster2",
#     "hebergeur": "ec2",
#     "nombreMaster": 1,
#     "nombreWorker": 1
#     }

# CLI: {\"nom\":\"testCluster3\"\,\"hebergeur\":\"ec2\"\,\"nombreMaster\":1\,\"nombreWorker\":1}

print("Hebergeur: ", clusterConfigData["hebergeur"])
# INSTANCES = { "nom": "nomCluster", "hebergeur": "nomHebergeur", "nombreMaster": 1, "nombreWorker": 3 }
# liste de [nom, nombre, type, role]
# ec2= t2.micro

# Constantes

IMAGES_ID = {
    "ec2":"ami-087855b6c8b59a9e4", # Ubuntu 18.04 LTS
    # "openstack": "ce02f059-e580-4228-8976-3e433d741619"
    "openstack": "ee602be9-30a1-489d-a78a-d5af8a2e7665"
}

SIZES_ID = {
    "ec2": "t2.micro",
    "openstack": "a1f42d75-efd6-42ba-b31f-76dc5e0770d0"
}

# Recuperation id & mdp de connexion

credentialsFile = open('credentialsCloud.csv', 'r')
credentialsString = credentialsFile.read()
credentialsList = credentialsString.split("\n")
credentialsList = [line.split(',') for line in credentialsList]
print(credentialsList)
# print(credentialsList)
CONNECTION_ID = ''
CONNECTION_PW = ''

for entry in credentialsList:
    if entry[0] == clusterConfigData["hebergeur"]:
        CONNECTION_ID = entry[1]
        CONNECTION_PW = entry[2]

if CONNECTION_ID == '' or CONNECTION_PW == '':
    raise RuntimeError("Recuperation des infos de connexion à l'hébergeur a échoué")

# import security
# libcloud.security.VERIFY_SSL_CERT = False

# Ouverture de la connexion avec l'hébergeur

driver = get_driver(clusterConfigData["hebergeur"])

if clusterConfigData["hebergeur"] == 'openstack':
    driverConnection = driver(CONNECTION_ID, CONNECTION_PW, 
        ex_force_auth_url='https://cloud-info.univ-lyon1.fr:5000/v3/auth/tokens', 
        ex_force_auth_version='3.x_password', 
        ex_tenant_name='INF3051L',
        ex_domain_name='UNIV-LYON1.FR',
        ex_tenant_domain_id="f938c31e567f4d57bcbde01a2a62c89d")
        
elif clusterConfigData["hebergeur"] == 'ec2':
    driverConnection = driver(CONNECTION_ID, CONNECTION_PW, region="eu-west-3")
else:
    driverConnection = driver(CONNECTION_ID, CONNECTION_PW)

# Recuperation des parametres necessaires à la creation de la machine

image = driverConnection.get_image(IMAGES_ID[clusterConfigData["hebergeur"]])
print("Image: ", image)
sizes = driverConnection.list_sizes()
size = [s for s in sizes if s.id == SIZES_ID[clusterConfigData["hebergeur"]]][0]
print("Size: ", size)

# Creation du deployment

with open("./MyFirstECTwo.pub", "r") as sshPublicKeyFile:
    sshPublicKey = sshPublicKeyFile.read()
    authSSHMachine = NodeAuthSSHKey(sshPublicKey)

# etapeAjoutCleSSH = SSHKeyDeployment(sshPublicKeyFile)
etapeAjoutClePrive = FileDeployment("./MyFirstECTwo.pem", "./.ssh/MyFirstECTwo.pem")
etapeAjoutScriptInstallation = FileDeployment("./scriptInstallationMachine.py", "./scriptsInstallation/scriptInstallationMachine.py")
etapeExecuteScriptInstallation = ScriptDeployment(script ="python3 ./scriptsInstallation/scriptInstallationMachine.py", name="scriptInstallationGenere")
deploymentMachine = MultiStepDeployment([etapeAjoutScriptInstallation, etapeAjoutClePrive, etapeExecuteScriptInstallation])
# deploymentMachine = MultiStepDeployment([etapeAjoutScriptInstallation, etapeExecuteScriptInstallation])

listNodes = []

for roleInstance in ("nombreMaster", "nombreWorker"):
    for numeroInstance in range (1, clusterConfigData[roleInstance] + 1):
        if roleInstance == "nombreMaster":
            lettre = "M"
        else:
            lettre = "W"
        name = "CC--" + clusterConfigData["nom"] + "--" + lettre + str(numeroInstance)
        print("Création instance: " + name)
        if clusterConfigData["hebergeur"] == "ec2":
            node = driverConnection.deploy_node(name=name, 
                image=image, size=size, 
                deploy=deploymentMachine, ssh_username="ubuntu", 
                auth=authSSHMachine, ex_security_groups=["projet-l3"], 
                ssh_key="./MyFirstECTwo.pem")
        elif clusterConfigData["hebergeur"] == "openstack":
            node = driverConnection.deploy_node(name=name, 
                image=image, size=size, 
                deploy=deploymentMachine, ssh_username="ubuntu", 
                ex_keyname="MyFirstECTwo", 
                ssh_key="./MyFirstECTwo.pem",
                ssh_interface="private_ips")
        print("Instance créé: " + name)
        listNodes.append(node)

# print(listNodes)
print(listNodes)

# Creation de cluster.yml pour RKE

ssh_path = "/home/ubuntu/.ssh/MyFirstECTwo.pem"

with open('./ressources/cluster.yml', 'r') as clusterBaseFile:
    cluster = yaml.load(clusterBaseFile)

    # print("Opened: ", yaml.dump(cluster, allow_unicode=True, default_flow_style=False))

for nodeIndex in range(len(listNodes)):
    if listNodes[nodeIndex].name.split("--")[2].startswith("M"):
        cluster["nodes"].append(cluster["nodes"][0])
        cluster["nodes"][nodeIndex + 2]["address"] = listNodes[nodeIndex].private_ips[0]
        cluster["nodes"][nodeIndex + 2]["hostname_override"] = listNodes[nodeIndex].name
        cluster["nodes"][nodeIndex + 2]["ssh_key_path"] = ssh_path
    else:
        cluster["nodes"].append(cluster["nodes"][1])
        cluster["nodes"][nodeIndex + 2]["address"] = listNodes[nodeIndex].private_ips[0]
        cluster["nodes"][nodeIndex + 2]["hostname_override"] = listNodes[nodeIndex].name
        cluster["nodes"][nodeIndex + 2]["ssh_key_path"] = ssh_path

# print("Before pop: ", cluster["nodes"])

cluster["nodes"].pop(0)
cluster["nodes"].pop(0)

cluster["ssh_key_path"] = ssh_path

# print("Finished: ", yaml.dump(cluster, allow_unicode=True, default_flow_style=False))

with open("./ressources/cluster_temp.yml", 'w') as clusterNewFile:
    yaml.dump(cluster, clusterNewFile)

listCommandsOpenstackAvantFTP = ["mkdir fichiersTemp"]
print(listCommandsOpenstackAvantFTP)
listCommandsOpenstackApresFTP = ["sudo cp ./fichiersTemp/environment_temp /etc/environment"]

# TODO: ajout des scripts pour faire marcher sur Openstack
if clusterConfigData["hebergeur"] == "openstack":
    for node in listNodes:

        # Connexion SSH
        client = paramiko.SSHClient()
        print("Loading system host keys")
        client.load_system_host_keys()
        print("System host keys loaded")
        client.set_missing_host_key_policy(paramiko.WarningPolicy())
        print("Warning policy for missing host key set")
        print("Opening SSH connection")
        client.connect(node.private_ips[0], username="ubuntu", key_filename="./MyFirstECTwo.pem")

        for command in listCommandsOpenstackAvantFTP:
            print("Executing ", command)
            stdin, stdout, stderr = client.exec_command(command)
            stdout = stdout.readlines()
            stderr = stderr.readlines()
            output = ""
            for line in stdout:
                output = output + line
            for line in stderr:
                output = output + line
            if output != "":
                print(output)

        # Creation du fichier env

        with open("./ressources/environment", "r") as envFile:
            env = envFile.readlines()

        print("Env: ", env)

        for index, line in enumerate(env):
            if line.startswith("no_proxy") or line.startswith("NO_PROXY"):
                print(line)
                line = line.replace('\n', "")
                for node in listNodes:
                    line = line + "," + node.private_ips[0]
                line = line + "\n"
                env[index] = line

        with open("./ressources/environment_temp", "w") as envFile:
            print(env)
            for line in env:
                envFile.write(line)

        # Envoi du fichier env
        print("Opening SFTP session")
        sftpsession = client.open_sftp()
        print("SFTP session opened")
        print("Sending environment_temp")
        sftpAtt2 = sftpsession.put("./ressources/environment_temp", "/home/ubuntu/fichiersTemp/environment_temp")
        print("File sent")
        print("Closing SFTP session")
        sftpsession.close()
        print("SFTP session closed")

        for command in listCommandsOpenstackApresFTP:
            print("Executing ", command)
            stdin, stdout, stderr = client.exec_command(command)
            stdout = stdout.readlines()
            stderr = stderr.readlines()
            output = ""
            for line in stdout:
                output = output + line
            for line in stderr:
                output = output + line
            if output != "":
                print(output)

        print("Closing connection")
        client.close()
        print("Connection closed")

# Installation de RKE et Kubernetes ainsi que pip3 et le script de gestion

listCommands = ("sudo apt-get update -y", 
    "sudo DEBIAN_FRONTEND=noninteractive apt-get install python3-pip -y",
    "pip3 install kubernetes",
    "pip3 install pyyaml",
    "mkdir gitRepos",
    "curl -L https://github.com/rancher/rke/releases/download/v1.0.0/rke_linux-amd64 -o rkeBinary",
    "chmod +x rkeBinary",
    "./rkeBinary up")

kubernetesDeployed = False

for node in listNodes:
    if node.name.split("--")[2] == "M1":

        client = paramiko.SSHClient()
        print("Loading system host keys")
        client.load_system_host_keys()
        print("System host keys loaded")
        client.set_missing_host_key_policy(paramiko.WarningPolicy())
        print("Warning policy for missing host key set")
        print("Opening SSH connection")
        if clusterConfigData["hebergeur"] == "openstack":
            client.connect(node.private_ips[0], username="ubuntu", key_filename="./MyFirstECTwo.pem")
        else:
            client.connect(node.public_ips[0], username="ubuntu", key_filename="./MyFirstECTwo.pem")
        #client.connect(node.public_ips[0], username="ubuntu", key_filename="./MyFirstECTwo.pem")
        print("Connection opened")
        print("Opening SFTP session")
        sftpsession = client.open_sftp()
        print("SFTP session opened")
        print("Sending cluster_temp.yml")
        sftpAtt = sftpsession.put("./ressources/cluster_temp.yml", "/home/ubuntu/cluster.yml")
        print("File sent")
        print("Sending scriptKubeAPI.py")
        sftpAtt2 = sftpsession.put("./scriptKubeAPI.py", "/home/ubuntu/scriptKubeAPI.py")
        print("File sent")
        print("Closing SFTP session")
        sftpsession.close()
        print("SFTP session closed")
        for command in listCommands:
            print("Executing ", command)
            stdin, stdout, stderr = client.exec_command(command)
            stdout = stdout.readlines()
            stderr = stderr.readlines()
            output = ""
            for line in stdout:
                output = output + line
            for line in stderr:
                output = output + line
            if output != "":
                print(output)

        # Add if kubectl is needed:
        # - mkdir ~/.kube
        # - cp kube_config_cluster.yml ~/.kube/config
        # - install kubectl

        print("Closing connection")
        client.close()
        print("Connection closed")

print("Kubernetes successfully installed")
print("Cluster ready for use")

