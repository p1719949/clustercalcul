import paramiko, argparse, json

# Creation du parser d'arguments

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command")
subparsers.required = True
parser_get = subparsers.add_parser('get')
parser_get.add_argument('--json', required=True)
parser_create = subparsers.add_parser('create') # Creation traité dans un autre fichier
parser_create.add_argument('--json', required=True)
parser_delete = subparsers.add_parser('delete')
parser_delete.add_argument('--json', required=True)
args = parser.parse_args()

print(args)

# Creation d'un objet python à partir du json

requestData = json.loads(args.json)
print("Arg: ", requestData)

# # Exemple de json pour get:
# requestData = {
#     "hebergeur": "ec2",
#     "nomCluster": "testCluster3",
#     "public_ips": [""],
#     "private_ips": [""]
# }

# CLI: {\"hebergeur\":\"ec2\",\"nomCluster\":\"testCluster3\",\"public_ips\":[\"35.180.60.89\"],\"private_ips\":[\"\"]}

# # Exemple de json pour delete:
# requestData = {
#     "hebergeur": "ec2",
#     "nomCluster": "testCluster3",
#     "public_ips": [""],
#     "private_ips": [""],
#     "resourceType": "",
#     "resourceName": ""
# }

# Determination de l'ip à utiliser
if requestData["hebergeur"] == "openstack":
    sshIp = requestData["private_ips"][0]
else:
    sshIp = requestData["public_ips"][0]

# Creation de la commande

if args.command == "get":
    requestCommand = "python3 scriptKubeAPI.py get"

if args.command == "delete":
    requestCommand = "python3 scriptKubeAPI.py delete --type=" + requestData["resourceType"] + " --name=" + requestData["resourceName"]

# Connexion ssh 

client = paramiko.SSHClient()
print("Loading system host keys")
client.load_system_host_keys()
print("System host keys loaded")
client.set_missing_host_key_policy(paramiko.WarningPolicy())
print("Warning policy for missing host key set")
print("Opening SSH connection")
client.connect(sshIp, username="ubuntu", key_filename="./MyFirstECTwo.pem")
print("Connection opened")
print("Executing ", requestCommand)
stdin, stdout, stderr = client.exec_command(requestCommand)
stdout = stdout.readlines()
stderr = stderr.readlines()
output = ""
for line in stdout:
    output = output + line
for line in stderr:
    output = output + line
if output != "":
    print(output)
print("Closing connection")
client.close()
print("Connection closed")
