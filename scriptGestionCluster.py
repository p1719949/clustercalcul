import json
import argparse

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.base import Node

from requests.exceptions import ConnectionError as REConnectionError

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command")
subparsers.required = True
parser_get = subparsers.add_parser('get')
parser_get.add_argument('--nomCluster')
parser_start = subparsers.add_parser('start')
parser_start.add_argument('--nomCluster', required=True)
parser_start = subparsers.add_parser('stop')
parser_start.add_argument('--nomCluster', required=True)
# parser_create = subparsers.add_parser('create') # Creation traité dans un autre fichier
# parser_create.add_argument('--json', required=True)
parser_delete = subparsers.add_parser('delete')
parser_delete.add_argument('--nomCluster', required=True)
args = parser.parse_args()

print("[INFO]" + str(args))

# Fonctions d'encodage JSON

def encode_node(node):
    if isinstance(node, Node):
        return {
            "uuid": node.uuid,
            "name": node.name,
            "state": str(node.state),
            "public_ips": node.public_ips,
            "private_ips": node.private_ips,
            "provider": node.driver.name
            # "size": node.size.name,
            # "image": node.image.name,
            # "created_at": node.created_at
            }
    else:
        type_name = node.__class__.__name__
        raise TypeError(f"Object of type '{type_name}' is not JSON serializable")

# Parametres à recuperer depuis le site:

# HEBERGEUR = 'ec2'
HEBERGEURS = ['ec2', 'openstack']
# HEBERGEURS = ['ec2']
# ec2, openstack

# Recuperation id & mdp de connexion

credentialsFile = open('credentialsCloud.csv', 'r')
credentialsString = credentialsFile.read()
credentialsList = credentialsString.split("\n")
credentialsList = [line.split(',') for line in credentialsList]
CONNECTION_ID = ''
CONNECTION_PW = ''
listMachinesCluster = []

driverConnDict = {}

for hebergeur in HEBERGEURS:

    for entry in credentialsList:
        if entry[0] == hebergeur:
            CONNECTION_ID = entry[1]
            CONNECTION_PW = entry[2]

    if CONNECTION_ID == '' or CONNECTION_PW == '':
        raise RuntimeError("Recuperation des infos de connexion à l'hébergeur a échoué")

    # Ouverture de la connexion avec l'hébergeur

    driver = get_driver(hebergeur)

    if hebergeur == 'openstack':
        driverConnection = driver(CONNECTION_ID, CONNECTION_PW,
            ex_force_auth_url='https://cloud-info.univ-lyon1.fr:5000/v3/auth/tokens', 
            ex_force_auth_version='3.x_password', ex_tenant_name='INF3051L',
            ex_domain_name='UNIV-LYON1.FR', ex_tenant_domain_id="f938c31e567f4d57bcbde01a2a62c89d")
    elif hebergeur == 'ec2':
        driverConnection = driver(CONNECTION_ID, CONNECTION_PW, region="eu-west-3")
    else:
        driverConnection = driver(CONNECTION_ID, CONNECTION_PW)

    driverConnDict[hebergeur] = driverConnection

    # listTypeMachines = driverConnection.list_sizes()

    # print(listTypeMachines)

    try:
        listMachinesCluster += driverConnection.list_nodes()
    except REConnectionError as e:
        if hebergeur == 'openstack':
            # print("Erreur: connection a openstack echoue", e)
            pass
        else:
            raise Exception("Erreur:", e)

    # print(listMachinesCluster)

print("[INFO]", driverConnDict)

if args.command == "get":
    if args.nomCluster == None:
        listCluster = []
        for node in listMachinesCluster:
            if node.name.startswith("CC--"):

                clusterDejaPresent = False
                for cluster in listCluster:
                    if node.name.split("--")[1] == cluster["nom"]:
                        clusterDejaPresent = True
                
                if node.state == "running":
                    enMarche = 1
                else:
                    enMarche = 0

                if clusterDejaPresent:
                    for cluster in listCluster:
                        if node.name.split("--")[1] == cluster["nom"]:
                            cluster["nombreTotalMachines"] += 1
                            cluster["nombreMachinesEnMarche"] += enMarche
                else:                   
                    listCluster.append(
                        {
                            "nom": node.name.split("--")[1],
                            "hebergeur": node.driver.name,
                            "nombreTotalMachines": 1,
                            "nombreMachinesEnMarche": enMarche
                        }
                    )
        jsonCluster = json.dumps(listCluster)
        print("[DATA]" + jsonCluster)
    else:
        print("[INFO] 0: ")
        listMachinesClusterCherche = []
        for node in listMachinesCluster:
            print("[INFO] 1:" + str(node.name))
            if node.name.startswith("CC--"):
                print("[INFO] 2:" + str(node.name))
                if node.name.split("--")[1] == args.nomCluster:
                    print("[INFO] 3:", node.state)
                    listMachinesClusterCherche.append(node)
        jsonMachinesCluster = json.dumps(listMachinesClusterCherche, default=encode_node)
        print("[DATA]" + jsonMachinesCluster)

if args.command == "start":
    startAttempts = 0
    startSuccessfulAttempts = 0
    for node in listMachinesCluster:
        if node.name.startswith("CC--"):
            if node.name.split("--")[1] == args.nomCluster:
                startAttempts += 1
                # print("Destroy node ", node.name)
                print("[INFO]", node)
                print("[INFO]", driverConnection)
                print("[INFO]", driverConnection.__dict__)
                print("[INFO]", driverConnection.__dir__)
                startSuccess = node.driver.start_node(node)
                if startSuccess:
                    startSuccessfulAttempts += 1
    startResults = {
        "nbreMachinesEssayes": startAttempts,
        "nbreMachinesReussis": startSuccessfulAttempts
    }
    jsonStartResults = json.dumps(startResults)
    print("[DATA]" + jsonStartResults)

if args.command == "stop":
    stopAttempts = 0
    stopSuccessfulAttempts = 0
    for node in listMachinesCluster:
        if node.name.startswith("CC--"):
            if node.name.split("--")[1] == args.nomCluster:
                stopAttempts += 1
                print("[INFO]Stop node ", node)
                stopSuccess = node.driver.stop_node(node)
                if stopSuccess:
                    stopSuccessfulAttempts +=1
    stopResults = {
        "nbreMachinesEssayes": stopAttempts,
        "nbreMachinesReussis": stopSuccessfulAttempts
    }
    jsonstopResults = json.dumps(stopResults)
    print("[DATA]" + jsonstopResults)
                
if args.command == "delete":
    deleteAttempts = 0
    deleteSuccessfulAttempts = 0
    for node in listMachinesCluster:
        if node.name.startswith("CC--"):
            if node.name.split("--")[1] == args.nomCluster:
                deleteAttempts += 1
                # print("Destroy node ", node.name)
                deleteSuccess = node.driver.destroy_node(node)
                if deleteSuccess:
                    deleteSuccessfulAttempts += 1
    deleteResults = {
        "nbreMachinesEssayes": deleteAttempts,
        "nbreMachinesReussis": deleteSuccessfulAttempts
    }
    jsondeleteResults = json.dumps(deleteResults)
    print("[DATA]" + jsondeleteResults)

# if args.command == "delete":
#     for node in listMachinesCluster:
#         if node.name.startswith("CC--"):
#             if node.name.split("--")[1] == args.nomCluster:
#                 print("Destroy node ", node.name)
#                 # driverConnection.destroy_node(node)
    
