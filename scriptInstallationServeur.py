import os

# Installation pip3 et libraires python

os.system("sudo apt-get update -y")
os.system("sudo DEBIAN_FRONTEND=noninteractive apt-get install python3-pip -y")
os.system("pip3 install apache-libcloud")
os.system("pip3 install paramiko")
os.system("pip3 install pyyaml")

# Installation docker

os.system("sudo apt-get update -y")
os.system("sudo DEBIAN_FRONTEND=noninteractive apt-get install -y   \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common")
os.system("curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -")
os.system("sudo apt-key fingerprint 0EBFCD88")
os.system("sudo add-apt-repository \
   \"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable\"")
os.system("sudo apt-get update -y")
os.system("sudo DEBIAN_FRONTEND=noninteractive apt-get install -y docker-ce=5:18.09.9~3-0~ubuntu-bionic docker-ce-cli=5:18.09.9~3-0~ubuntu-bionic containerd.io")

# Installation du serveur node

os.system("sudo apt-get install node")
os.system("sudo apt-get install npm")
os.system("cd siteweb")
os.system("npm install")

# Amelioration: Creation registry