from kubernetes import client, config
import argparse, json, yaml

# Initialisation du parser de commandes
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command")
subparsers.required = True
parser_get = subparsers.add_parser('get')
parser_create = subparsers.add_parser('create')
parser_create.add_argument('--type', choices=['deployment'], required=True)
parser_create.add_argument('--fileLocation', required=True)
parser_delete = subparsers.add_parser('delete')
parser_delete.add_argument('--type', choices=['deployment'], required=True)
parser_delete.add_argument('--name', required=True)
args = parser.parse_args()

print(args)

# Initialisation des variables K8S

# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config("/home/ubuntu/kube_config_cluster.yml")

AppsV1Api = client.AppsV1Api()

# Traitement des commandes

if args.command == "get":
    listResources = []
    currentDeployments = AppsV1Api.list_namespaced_deployment(namespace="default")
    for deployment in currentDeployments.items:
        # print(deployment.metadata.name)
        listResources.append({
            "type": "deployment",
            "name": deployment.metadata.name
        })
    # print(listResources)
    # print("Final result: ")
    print(json.dumps(listResources, indent=4))
    
if args.command == "create":
    with open(args.fileLocation) as f:
        deploymentBody = yaml.safe_load(f)
        print(deploymentBody)
        if deploymentBody["kind"].lower() == "Deployment".lower():
            deployment = AppsV1Api.create_namespaced_deployment("default", deploymentBody)
        print(deployment) # Test

if args.command == "delete":
    if args.type == "deployment":
        deleteStatus = AppsV1Api.delete_namespaced_deployment(args.name, "default")
        print(deleteStatus) # Test