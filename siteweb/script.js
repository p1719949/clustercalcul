/* Hello world */

const local_server = "http://localhost:8080"
const server = local_server;

function ouvrirTab(nomTab, bouton) {
    // https://www.w3schools.com/howto/howto_js_full_page_tabs.asp

    console.log('Appel à ouvrirTab');

    //on fait un dictionaire pour changer l'url selon quel onglet est ouvert
    const dictionaire_titre_onglet = {creation:"/nullcreation",ajout:"/nullajout",gestion:"/nullgestion"};
    
    // On cache tous les tabs
    let contenuTabs = document.getElementsByClassName("tabContenu");
    for (let i = 0; i < contenuTabs.length; i++) {
        contenuTabs[i].style.display = "none";
    }

    // On remet tous les boutons à la couleur initiale
    let boutonsTab = document.getElementsByClassName("tabBouton");
    for (let j = 0; j < boutonsTab.length; j++) {
        boutonsTab[j].style.backgroundColor = '';
        boutonsTab[j].style.color = '';
    }

    // On affiche le tab selectionné
    document.getElementById(nomTab).style.display = "block";
    const newturl = window.location.href.match(/([^/]*\/){6}/) || "";
    history.pushState({id: nomTab},nomTab,newturl + nomTab);

    // Le bouton du tab selectionné est changé
    bouton.style.backgroundColor = 'white';
    bouton.style.color = 'black';

    if (nomTab === "gestion" && document.getElementById("chargementCluster").style.display != "none") {
        // affichageMachines = Promise.all([server_get_machines()])
        // .then(data => {
        //     // console.log(data[0]);
        //     listeClusterHTML = "";
        //     for (cluster of data[0]) {
        //         console.log(cluster);
        //         listeClusterHTML += clusterToHTML(cluster);
        //     }
        //     document.getElementById("listeCluster").innerHTML = listeClusterHTML;
        //     document.getElementById("chargementCluster").style.display = "none";
        // });
        getClusters(true);
    }
}

function getClusters(chargementInitial) {
    console.log('appel getClusters')

    let headers = new Headers();
    headers.set("Content-Type", "application/json");

    affichageCluster = fetch(server + '/clusters', {method: "GET", headers: headers})
        .then(response => response.json())
        .then(data => {
            // console.log(data[0]);
            listeClusterHTML = "";
            for (cluster of data) {
                listeClusterHTML += clusterToHTML(cluster);
            }
            document.getElementById("listeCluster").innerHTML = listeClusterHTML;
            // for (span of document.getElementsByClassName("fermetureModal")) {
            //     span.onclick = function() {
            //         console.log(span.parentElement.parentElement);
            //         console.log(span.parentElement.parentElement.style.display);
            //         console.log(span.parentElement);
            //         console.log((span.parentElement.parentElement).style.display);
            //         (span.parentElement.parentElement).style.display = "none";
            //     }
            // }
            // for (cluster of data) {
            //     id = "modalDeleteCluster" + cluster.nom
            //     document.getElementById("fermetureModalDeleteCluster" + cluster.nom).onclick = function() {
            //         document.getElementById(id).style.display = "none";
            //     }
            // }
            if (chargementInitial === true) {
                document.getElementById("chargementCluster").style.display = "none";
            }
            else {
                document.getElementsByClassName("etatRafraichissement")[0].innerHTML = "";
                document.getElementsByClassName("etatRafraichissement")[0].innerHTML = '<image src="icones/icons8-checkmark-26.png">';
                setTimeout(function() {
                    document.getElementsByClassName("etatRafraichissement")[0].innerHTML = '';
                }, 5000);
            }
        });
    if (chargementInitial === false) {
        document.getElementsByClassName("etatRafraichissement")[0].innerHTML =
        '<div class="smallLoader"></div>';
    }
}

function clusterToHTML(cluster) {
    let nombreMachines = 0;
    let nombreMachinesRunning = 0;
    html = `
    <div class="cluster">
      <div class="titreCluster">
        <p class="nomCluster">${cluster.nom}</p>
      </div>
      <p class="donneesCluster">${cluster.nombreMachinesEnMarche} sur ${cluster.nombreTotalMachines} machines en marche</br>
      Hébergé sur ${cluster.hebergeur}</p>
      <div class="boutonsOperationsCluster">
        <button class="boutonOpCluster" onclick="startStopCluster('start', '${cluster.nom}')" id="boutonStartCluster">Démarrer</button>
        <button class="boutonOpCluster" onclick="startStopCluster('stop', '${cluster.nom}')" id="boutonStopCluster">Arrêter</button>
        <button class="boutonOpCluster" onclick="ouvrirModalDelete('${cluster.nom}');" id="boutonDeleteCluster">Supprimer</button>
        <div id="modalDeleteCluster${cluster.nom}" class="modalDeleteCluster">
          <!-- Modal content -->
          <div class="contenuModalDeleteCluster" id="contenuModalDeleteCluster${cluster.nom}">
            <!--<span class="fermetureModal" id="fermetureModalDeleteCluster${cluster.nom}">&times;</span>-->
            <p>Etes-vous sûr de vouloir supprimer le cluster ${cluster.nom}?</p>
            <button class="boutonModalDeleteCluster" onclick="deleteCluster('${cluster.nom}')">Supprimer</button>
            <button class="boutonModalDeleteCluster" onclick="fermerModalDelete('${cluster.nom}')">Annuler</button>
          </div>
        </div>
      </div>
        <button class="boutonsInfosCluster" onclick="getMachines('${cluster.nom}')" id="boutonMachinesCluster">Machines</button>
        <button class="boutonsInfosCluster" onclick="" id="boutonKubernetesCluster">Kubernetes</button>
        <div class="infosCluster" id="infoCluster${cluster.nom}"></div> 
      </div> `;

      // deleteCluster('${cluster.nom}')

    //   <p>Machines:</p>
    //   <table class="machine">
    //     <tr class="machine">
    //       <th class="machine">Nom</th>
    //       <th class="machine">Etat</th>
    //       <th class="machine">Adresse IP</th>
    //       <!--<th>Action<th>-->
    //     </tr>`;
    // for (machine of cluster.listeMachines) {
    //     console.log(machine);
    //     html += '<tr class="machine">' + machineToHTML(machine) + '</tr>';
    // }

    // html += `
    //   </table> 
    // </div>
    // `;
    html += `</div>`
    return html;
}

function ouvrirModalDelete(nomCluster) {
    document.getElementById("modalDeleteCluster" + nomCluster).style.display = "block";
}

function fermerModalDelete(nomCluster) {
    document.getElementById("modalDeleteCluster" + nomCluster).style.display = "none";
}

function getMachines(nomCluster) {
    console.log('appel getMachines')

    let headers = new Headers();
    headers.set("Content-Type", "application/json");

    affichageCluster = fetch(server + '/clusters/' + nomCluster, {method: "GET", headers: headers})
        .then(response => response.json())
        .then(data => {
            console.log(data);
            listeMachineHTML = `
              <table class="machine">
                <tr class="machine">
                  <th class="machine">Nom</th>
                  <th class="machine">Etat</th>
                  <th class="machine">Adresse IP</th>
                  <!--<th>Action<th>-->
                </tr>`;
            for (machine of data) {
                console.log(machine);
                listeMachineHTML += '<tr class="machine">' + machineToHTML(machine) + '</tr>';
            }
            console.log(listeMachineHTML);

            listeMachineHTML += `
              </table> 
            </div>
            `;
            document.getElementById("infoCluster"+nomCluster).innerHTML = listeMachineHTML
        });
        document.getElementById("infoCluster"+nomCluster).innerHTML =
        '<div class="smallLoader"></div>';
}

function machineToHTML(node) {
    let adresseIP = "";
    if (node.state == "running") {
        console.log("hey");
        if (node.provider == "OpenStack") {
            console.log(node.private_ips);
            adresseIP = node.private_ips[0];
        }
        else {
            adresseIP = node.public_ips[0];
        }
    }
    return `
    <td class="machine">${node.name.split("--")[2]}</td>
    <td class="machine">${node.state}</td>
    <td class="machine">${adresseIP}</td>
    <!--<td>Démarrer</td>-->
    `;
}

function startStopCluster(type, nomCluster) {
    console.log("Appel startStopCluster");

    let headers = new Headers();
    headers.set("Content-Type", "application/json");

    requete = fetch(server + '/clusters/' + nomCluster + "/" + type, {method: "POST", headers: headers})
    .then(response => response.json())
    .then(data => {
        typeAlerte = '';
        nomAlerte = '';
        verbe = '';
        if (type === 'start') {
            verbe = 'démarrées';
        }
        else {
            verbe = 'éteintes';
        }
        // console.log(data);
        if (data.nbreMachinesReussis === 0) {
            nomAlerte= 'Erreur';
            typeAlerte = 'Erreur';
        }
        if (data.nbreMachinesReussis !== 0 && data.nbreMachinesEssayes !== data.nbreMachinesReussis) {
            nomAlerte = 'Warning';
            typeAlerte = 'Warning';
        }
        if (data.nbreMachinesEssayes === data.nbreMachinesReussis) {
            nomAlerte = 'Succès';
            typeAlerte = 'Succes';
        }
        affichageFinal = `
        <div class="alerte alerte${typeAlerte}">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            <strong>${nomAlerte}:</strong> ${data.nbreMachinesReussis}/${data.nbreMachinesEssayes} machines ${verbe}.
        </div>
        `;
        document.getElementById("etatOperationCluster").innerHTML = affichageFinal;
    })
    .catch(error => {
        console.log(error);
    });

    message = ' de ' + nomCluster;
    if (type === 'start') {
        message = 'Démarrage' + message;
    }
    else {
        message = 'Arrêt' +message;
    }
    document.getElementById("etatOperationCluster").innerHTML = `
    <div class="smallLoader"></div>
    <p>${message}</p>
    `;
}

function createCluster() {
    console.log("Appel createCluster()");

    // document.getElementsByName("formCreationCluster")[0].preventDefault();

    let headers = new Headers();
    headers.set("Content-Type", "application/json");
    const body = JSON.stringify({
        nom: document.getElementById("nom").value,
        hebergeur: document.getElementById("hebergeur").value,
        nombreMaster: document.getElementById("nombreMaster").value,
        nombreWorker: document.getElementById("nombreWorker").value
    });

    requete = fetch(server + '/clusters', {method: "POST", headers: headers, body:body})
    .then(response => {
        if (response.ok) {
            document.getElementsByClassName("etatCreation")[0].innerHTML = `
            <div class="alerte alerteSucces">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Succes:</strong> Creation du cluster ${document.getElementById("nom").value} a réussi.
            </div>
            `;
        }
        else {
            document.getElementsByClassName("etatCreation")[0].innerHTML = `
            <div class="alerte alerteErreur">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Erreur:</strong> Creation du cluster ${document.getElementById("nom").value} a échoué.
            </div>
            `;
        }
        document.getElementById("boutonSubmitCreation").disabled = false;
    })
    .catch(error => console.log(error));

    ouvrirTab("creation", document.getElementById("crea"));
    document.getElementsByClassName("etatCreation")[0].innerHTML = `
    <div class="smallLoader"></div>
    <p>Lancement de ${document.getElementById("nom").value}</p>
    `;
    document.getElementById("boutonSubmitCreation").disabled = true;

}

function deleteCluster(nomCluster) {
    console.log("Appel deleteCluster");

    fermerModalDelete(nomCluster);

    let headers = new Headers();
    headers.set("Content-Type", "application/json");

    requete = fetch(server + '/clusters/' + nomCluster, {method: "DELETE", headers: headers})
    .then(response => response.json())
    .then(data => {
        // console.log(data);
        if (data.nbreMachinesReussis === 0) {
            nomAlerte= 'Erreur';
            typeAlerte = 'Erreur';
        }
        if (data.nbreMachinesReussis !== 0 && data.nbreMachinesEssayes !== data.nbreMachinesReussis) {
            nomAlerte = 'Warning';
            typeAlerte = 'Warning';
        }
        if (data.nbreMachinesEssayes === data.nbreMachinesReussis) {
            nomAlerte = 'Succès';
            typeAlerte = 'Info';
        }
        affichageFinal = `
        <div class="alerte alerte${typeAlerte}">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            <strong>${nomAlerte}:</strong> ${data.nbreMachinesReussis}/${data.nbreMachinesEssayes} machines supprimés.
        </div>
        `;
        document.getElementById("etatOperationCluster").innerHTML = affichageFinal;
    })
    .catch(error => {
        console.log(error);
    });

    document.getElementById("etatOperationCluster").innerHTML = `
    <div class="smallLoader"></div>
    <p>Suppression de ${nomCluster}</p>
    `;
}

// function server_get_clusters() {
//     console.log(`Calling server_get_machines()`);

//     let headers = new Headers();
//     headers.set("Content-Type", "application/json");

//     return fetch(server + '/clusters', {method: "GET", headers: headers})
//     .then(response => response.json())
//     .then(data => {
//         // console.log(data);
//         return data;
//     })
//     .catch(error => console.log(error));
// }

document.addEventListener("DOMContentLoaded", function() {
    console.log("DOM content loaded");
    const href = window.location.href.split('/').pop();
    if(href == "gestion")
    {
        document.getElementById("ges").click()
    }
    else
        {
        if(href == "creation")
        {
            document.getElementById('crea').click()
        }
        else
        {
            document.getElementById('aj').click()
        }
    }
}, false);

