// Imports
const express = require('express');
const spawn = require("child_process").spawn;
const path = require("path");
const bodyParser = require('body-parser');
const history = require('history');
const server  = express();

//server.use(bodyParser.urlendcoded({extended: true}));

server.use(express.static(__dirname));
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());

// Requetes de recuperation de la page

server.get('/', (req,res)=> {
    res.sendFile(path.join(__dirname,'index.html'))
});

// redirection pour les refresh dans les onglets.

server.get('/ajout',(req,res)=>{
    res.sendFile(path.join(__dirname,'index.html'))
})

server.get('/gestion',(req,res)=>{
    res.sendFile(path.join(__dirname,'index.html'))
})

server.get('/creation',(req,res)=>{
    res.sendFile(path.join(__dirname,'index.html'))
})

// API Clusters

server.get('/clusters', (req, res) => {
    console.log("GET clusters")
    
    const process = spawn('python3', ["./scriptGestionCluster.py", "get"], {cwd: ".."});
    // const process = spawn('ls', ['..']);
    process.stdout.on('data', data => {
        console.log(`stdout: ${data}`);
        // res.send(data.toString());
        for (line of data.toString().split("\n")) {
            if (line.startsWith("[DATA]")) {
                res.json(JSON.parse(line.replace("[DATA]", "")));
            }
        }
    });
    process.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
        res.sendStatus(500);
    });
    process.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
    // console.log("Hello");
    
});

server.get('/clusters/:name', (req, res) => {
    console.log("GET clusters " + req.params.name)
    
    const process = spawn('python3', ["./scriptGestionCluster.py", "get", "--nomCluster", req.params.name], {cwd: ".."});
    // const process = spawn('ls', ['..']);
    process.stdout.on('data', data => {
        console.log(`stdout: ${data}`);
        // res.send(data.toString());
        for (line of data.toString().split("\n")) {
            if (line.startsWith("[DATA]")) {
                res.json(JSON.parse(line.replace("[DATA]", "")));
            }
        }
    });
    process.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
        res.sendStatus(500);
    });
    process.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
    // console.log("Hello");
    
});

server.post('/clusters',(req,res) => {
    console.log("POST clusters");
    console.log(req.body);
    req.body.nombreMaster = parseInt(req.body.nombreMaster, 10);
    req.body.nombreWorker = parseInt(req.body.nombreWorker, 10);
    console.log(JSON.stringify(req.body));
    const process = spawn('python3', ["./scriptCreationCluster.py", JSON.stringify(req.body)], {cwd: ".."});
    process.stdout.on('data', data => {
        console.log(`stdout: ${data}`);
    });
    process.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });
    process.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
        if (code === 0) {
            res.sendStatus(202);
        }
        else {
            res.sendStatus(500);
        }
    });
    // res.sendFile(path.join(__dirname,"index.html")) //placeholder
});

server.post('/clusters/:name/start', (req, res) => {
    console.log("POST clusters " + req.params.name + " start")
    
    const process = spawn('python3', ["./scriptGestionCluster.py", "start", "--nomCluster", req.params.name], {cwd: ".."});
    // const process = spawn('ls', ['..']);
    process.stdout.on('data', data => {
        console.log(`stdout: ${data}`);
        // res.send(data.toString());
        for (line of data.toString().split("\n")) {
            if (line.startsWith("[DATA]")) {
                res.json(JSON.parse(line.replace("[DATA]", "")));
            }
        }
    });
    process.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
        res.sendStatus(500);
    });
    process.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
    // console.log("Hello");
    
});

server.post('/clusters/:name/stop', (req, res) => {
    console.log("POST clusters " + req.param.name + " stop")
    
    const process = spawn('python3', ["./scriptGestionCluster.py", "stop", "--nomCluster", req.params.name], {cwd: ".."});
    // const process = spawn('ls', ['..']);
    process.stdout.on('data', data => {
        console.log(`stdout: ${data.toString()}`);
        // res.send(data.toString());
        for (line of data.toString().split("\n")) {
            if (line.startsWith("[DATA]")) {
                res.json(JSON.parse(line.replace("[DATA]", "")));
            }
        }
    });
    process.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
        res.sendStatus(500);
    });
    process.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
    // console.log("Hello");
    
});

server.delete('/clusters/:name', (req, res) => {
    console.log("DELETE clusters " + req.param.name)
    
    const process = spawn('python3', ["./scriptGestionCluster.py", "delete", "--nomCluster", req.params.name], {cwd: ".."});
    // const process = spawn('ls', ['..']);
    process.stdout.on('data', data => {
        console.log(`stdout: ${data}`);
        // res.send(data.toString());
        for (line of data.toString().split("\n")) {
            if (line.startsWith("[DATA]")) {
                res.json(JSON.parse(line.replace("[DATA]", "")));
            }
        }
    });
    process.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
        res.sendStatus(500);
    });
    process.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
    // console.log("Hello");
    
});

// server.post('/creation',(req,res) =>{
//     console.log(req.body);                          //req.body.number pour ne renvoyer que number
//     res.sendFile(path.join(__dirname,"index.html"))
// });

// server.post('/ajout', (req,res) =>{
//     console.log(req.body);
//     res.sendFile(path.join(__dirname,"index.html")) //placeholder to replace with python script
// });

// Initialisation Serveur

server.listen(8080, () => console.log(`Server is listening on port 8080`));   





